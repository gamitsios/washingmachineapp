package com.example.washingmachineapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    /**
     * tag for debugging reasons
     */
    private static final String TAG = "ACTIVITY_MAIN";

    /**
     * these strings are used across the application to
     * save the preference of the user, if he wants speech guidance or not
     */
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String SHOW_DIALOG = "showDialog";
    public static final String EXP_USER = "expUser";
    MediaPlayer sound1;
    MediaPlayer sound2;
    Animation from_bottom, from_top, main_view_tv_animation;
    ImageView logo;
    TextView mainTv1, mainTv2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boolean cleanStart = getIntent().getBooleanExtra("cleanStart",true);
        Log.d(TAG, "cleanStart: " + cleanStart + "");



        Toolbar mToolbar =  (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar (mToolbar);

        //declare sounds
        sound1 = MediaPlayer.create(MainActivity.this, R.raw.basikesepiloges);
        sound2 = MediaPlayer.create(MainActivity.this, R.raw.synthetesepiloges);

         //declare animations
        from_bottom = AnimationUtils.loadAnimation(this, R.anim.from_bottom);
        from_top = AnimationUtils.loadAnimation(this, R.anim.from_top);
        main_view_tv_animation = AnimationUtils.loadAnimation(this, R.anim.main_view_tv_animation);

        //declare text views
        mainTv1 = (TextView) findViewById(R.id.mainTv1);
        mainTv2 = (TextView) findViewById(R.id.mainTv2);

        //start animations
        mainTv1.startAnimation(main_view_tv_animation);
        mainTv2.startAnimation(main_view_tv_animation);

        logo = (ImageView) findViewById(R.id.logo);
        logo.setAnimation(from_top);


        /*
         * create 2 SharedPreferences variables to save the instance of the app, one to check if the alert dialog will show
         * and the other to see if the user wants speech guidance
         */
        final SharedPreferences sharedPreferencesForDialog = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        final SharedPreferences sharedPreferencesForSpeech = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        sharedPreferencesForSpeech.getBoolean(EXP_USER,false);


        /*
         * the default boolean of the sharePReferenceFor dialog is true
         * show a message to check if the user wants speech
         */

        if (sharedPreferencesForDialog.getBoolean(SHOW_DIALOG, true)){
            AlertDialog alert = new AlertDialog.Builder(MainActivity.this).create();
            alert.setTitle("Πληροφορίες Συστήματος");
            alert.setMessage("Παρακαλώ επιλέξτε αν θέλετε ηχητική βοήθεια");
            alert.setCanceledOnTouchOutside(false);
            alert.setCancelable(false);
            alert.setButton(AlertDialog.BUTTON_POSITIVE, "ΝΑΙ",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            /*
                             * if the user presses yes then the global boolean for speech turns true and the dialog will not show again until
                             * it is reset
                             */
                            sharedPreferencesForDialog.edit().putBoolean(SHOW_DIALOG, false).apply();
                            sharedPreferencesForSpeech.edit().putBoolean(EXP_USER, true).apply();
                            dialog.dismiss();

                            /*
                             * play the sounds
                             */
                            sound1.start();
                            sound1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    mp.reset();
                                    sound2.start();
                                    sound2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        @Override
                                        public void onCompletion(MediaPlayer mp) {
                                            mp.reset();
                                        }
                                    });
                                }
                            });

                        }
                    });

            /*
             * if the user presses no then the global boolean for speech is false and no sound will play.
             */
            alert.setButton(AlertDialog.BUTTON_NEGATIVE, "OΧΙ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sharedPreferencesForDialog.edit().putBoolean(SHOW_DIALOG, false).apply();
                    sharedPreferencesForSpeech.edit().putBoolean(EXP_USER, false).apply();
                    dialog.dismiss();
                }
            });
            alert.setIcon(R.drawable.my_info_dialog_img);
            alert.show();
            TextView message = (TextView)alert.findViewById(android.R.id.message);
            message.setTextSize(40);

            TextView buttonYes = (TextView)alert.findViewById(android.R.id.button1);
            buttonYes.setTextSize(40);

            TextView buttonNo = (TextView)alert.findViewById(android.R.id.button2);
            buttonNo.setTextSize(40);




        }
        /**
         * everytime the app launches and the user has agreed to speech tha sounds will play
         */

        if(sharedPreferencesForSpeech.getBoolean(EXP_USER, false) && cleanStart){
            sound1.start();
            sound1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.reset();
                    sound2.start();
                    sound2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            mp.reset();
                        }
                    });
                }
            });
        }

        Log.d(TAG, "sharedPreferences for dialog: " + sharedPreferencesForDialog.getBoolean(SHOW_DIALOG, true) +
                " sharedPreferences for speech: " + sharedPreferencesForSpeech.getBoolean(EXP_USER, true));





        final Button button1 = (Button)findViewById(R.id.aButton1);
        button1.setAnimation(from_bottom);
        button1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if (sound1 != null){
                    sound1.release();
                }
                if (sound2 != null){
                    sound2.release();
                }
                Intent proepilogesIntent = new Intent(v.getContext(),BasikesEpilogesActivity.class);
                proepilogesIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(proepilogesIntent);



            }
        });

       final Button button2 = (Button)findViewById(R.id.abutton2);
       button2.setAnimation(from_bottom);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sound1 != null){
                    sound1.release();
                }
                if (sound2 != null){
                    sound2.release();
                }
                Intent synthetesEpilogesIntent = new Intent(v.getContext(),SynthetesEpilogesActivity.class);
                synthetesEpilogesIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(synthetesEpilogesIntent);
            }
        });

        final Button button3 = (Button)findViewById(R.id.aButton3);
        button3.setAnimation(from_bottom);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sound1 != null){
                    sound1.release();
                }
                if (sound2 != null){
                    sound2.release();
                }
                Intent rithmisiProgrammatosIntent = new Intent(v.getContext(),RithmisiProgrammatosActivity.class);
                rithmisiProgrammatosIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(rithmisiProgrammatosIntent);

            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == R.id.menu_reset){
            SharedPreferences sharedPreferencesForDialog = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
            SharedPreferences sharedPreferencesForSpeech = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
            sharedPreferencesForDialog.edit().putBoolean(SHOW_DIALOG, true).apply();
            sharedPreferencesForSpeech.edit().putBoolean(EXP_USER, false).apply();
            recreate();
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onStop() {
        if (sound1 != null){
            sound1.release();
        }
        if (sound2 != null){
            sound2.release();
        }

        super.onStop();
    }


}

