package com.example.washingmachineapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import static com.example.washingmachineapp.MainActivity.EXP_USER;
import static com.example.washingmachineapp.MainActivity.SHARED_PREFS;

public class PlisiActivity extends AppCompatActivity {

    MediaPlayer sound1, sound2, sound3;


    private CountDownTimer countDownTimer;
    private Boolean mTimerRunning;
    private long mTimeLeftMillis;
    TextView timerValue, desc1, desc2, desc3, desc4, typeTextView, speedTextView, tempTextView, addInfoTextView ;
    SharedPreferences sharedPreferencesForSpeech;
    Animation watchAnimation, roundAnimation;
    ImageView imgTimer, imgTimer2;
    View divpage;
    AlertDialog alertDialog;
    Button startPauseButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plisi);



        sharedPreferencesForSpeech = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        //declare animations
        watchAnimation = AnimationUtils.loadAnimation(this, R.anim.watch_animation);
        roundAnimation = AnimationUtils.loadAnimation(this, R.anim.round_animation);

        //declare text views
        timerValue = (TextView) findViewById(R.id.timerView);
        desc1 = (TextView) findViewById(R.id.desc1);
        desc2 = (TextView) findViewById(R.id.desc2);
        desc3 = (TextView) findViewById(R.id.desc3);
        desc4 =(TextView) findViewById(R.id.desc4);
        typeTextView = (TextView) findViewById(R.id.typeTextView);
        speedTextView = (TextView) findViewById(R.id.speedTextView);
        tempTextView = (TextView) findViewById(R.id.tempTextView);
        addInfoTextView = (TextView) findViewById(R.id.addInfoTextView);

        //declare buttons

        startPauseButton = (Button) findViewById(R.id.startPauseButton);


        //declare image views
        imgTimer = (ImageView) findViewById(R.id.imgTimer1);
        imgTimer2 = (ImageView) findViewById(R.id.imgTimer2);

        //declare views
        divpage = (View) findViewById(R.id.divpage);

        //start animations
        timerValue.startAnimation(watchAnimation);
        imgTimer.startAnimation(watchAnimation);
        imgTimer2.startAnimation(watchAnimation);
        divpage.startAnimation(watchAnimation);
        desc1.startAnimation(watchAnimation);
        desc2.startAnimation(watchAnimation);
        desc3.startAnimation(watchAnimation);
        desc4.startAnimation(watchAnimation);
        typeTextView.startAnimation(watchAnimation);
        speedTextView.startAnimation(watchAnimation);
        tempTextView.startAnimation(watchAnimation);
        addInfoTextView.startAnimation(watchAnimation);
        startPauseButton.startAnimation(watchAnimation);

        //declare click listener on playPause button
        startPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTimerRunning){
                    startPauseButton.setText("Έναρξη Πλύσης");
                    startPauseButton.setCompoundDrawablesWithIntrinsicBounds(null, null, ResourcesCompat.getDrawable(getResources(), R.drawable.ic_media_play, null), null);
                    pauseTimer();
                }else {
                    startPauseButton.setText("Παύση Πλύσης");
                    startPauseButton.setCompoundDrawablesWithIntrinsicBounds(null, null, ResourcesCompat.getDrawable(getResources(), R.drawable.ic_media_pause, null), null);
                    startTimer(mTimeLeftMillis);
                }

            }
        });


        long washTimeMillis = 0;

        //declare toolbar
        Toolbar mToolbar =  (Toolbar) findViewById(R.id.myToolbar);
        mToolbar.setTitle("Πρόγραμμα Πλύσης");
        setSupportActionBar (mToolbar);

        //get the boolean cleanStart
        String selection = getIntent().getStringExtra("message");

        //go to activity from default
        if (selection!=null){
            ProgrammaPlisis prog = new ProgrammaPlisis(selection);
            speechSelection(prog.getType());
            washTimeMillis = (long) prog.getTime() * 60 * 1000;
            typeTextView.setText(" " + prog.getType() + " ");
            speedTextView.setText(" " + prog.getSpeed() + " ");
            tempTextView.setText(" " + prog.getTemperature() + " ");

         //go to activity from rithmisi programmatos
        }else{
            ProgrammaPlisis prog = (ProgrammaPlisis) getIntent().getSerializableExtra("message1");
            speechSelection(prog.getType());
            washTimeMillis = (long) prog.getTime() * 60 * 1000;
            typeTextView.setText(" " + prog.getType() + " ");
            speedTextView.setText(" " + prog.getSpeed() + " ");
            tempTextView.setText(" " + prog.getTemperature() + " ");
            if (!prog.getAdditionalInfo().isEmpty()){
                desc4.setText("Επιπρόσθετα:");
                addInfoTextView.setText(prog.convertAdditionalInfo(prog.getAdditionalInfo()));
            }

        }
        startTimer(washTimeMillis);
    }

    private void speechSelection(String type){
        if (sharedPreferencesForSpeech.getBoolean(EXP_USER, true)){
            switch (type){
                case "Πρόγραμμα Γρήγορης Πλύσης":

                case "Γρήγορη Πλύση":
                    sound1 = MediaPlayer.create(PlisiActivity.this, R.raw.programma_grigoris_plisis);
                    sound2 = MediaPlayer.create(PlisiActivity.this, R.raw.time_30_mins);
                    break;
                case "Οικονομικό Πρόγραμμα":
                    sound1 = MediaPlayer.create(PlisiActivity.this, R.raw.programma_oikonomiko);
                    sound2 = MediaPlayer.create(PlisiActivity.this, R.raw.time_110_mins);

                    break;

                case "Κανονικό Πρόγραμμα" :
                    sound1 = MediaPlayer.create(PlisiActivity.this, R.raw.programma_kanoniko);
                    sound2 = MediaPlayer.create(PlisiActivity.this, R.raw.time_60_mins);
                    break;

                case "Βαμβακερά" :
                    sound1 = MediaPlayer.create(PlisiActivity.this, R.raw.programma_bambakera);
                    sound2 = MediaPlayer.create(PlisiActivity.this, R.raw.time_125_mins);
                    break;

                case "Συνθετικά" :
                    sound1 = MediaPlayer.create(PlisiActivity.this, R.raw.programma_synthetika);
                    sound2 = MediaPlayer.create(PlisiActivity.this, R.raw.time_110_mins);
                    break;


                case "Μάλλινα":
                    sound1 = MediaPlayer.create(PlisiActivity.this, R.raw.programma_mallina);
                    sound2 = MediaPlayer.create(PlisiActivity.this, R.raw.time_45_mins);
                    break;

                case "Ευαίσθητα":
                    sound1 = MediaPlayer.create(PlisiActivity.this, R.raw.programma_euesthita);
                    sound2 = MediaPlayer.create(PlisiActivity.this, R.raw.time_45_mins);
                    break;

                case "Λευκά" :
                    sound1 = MediaPlayer.create(PlisiActivity.this, R.raw.programma_leuka);
                    sound2 = MediaPlayer.create(PlisiActivity.this, R.raw.time_120_mins);
                    break;
            }


                sound1.start();
                sound1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.reset();
                        sound2.start();
                        sound2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                mp.reset();
                            }
                        });
                    }
                });


        }



    }


    private void startTimer(long washTimeMillis){
        mTimeLeftMillis = washTimeMillis;
        //mTimeLeftMillis = 10000;
        countDownTimer = new CountDownTimer(mTimeLeftMillis, 1000) {
           @Override
           public void onTick(long millisUntilFinished) {
               mTimeLeftMillis = millisUntilFinished;
               updateCountDownText();

           }

           @Override
           public void onFinish() {
               sound3 = MediaPlayer.create(PlisiActivity.this, R.raw.plisi_done);
               if (sharedPreferencesForSpeech.getBoolean(EXP_USER, true)){
                   sound3.start();
                   sound3.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                       @Override
                       public void onCompletion(MediaPlayer mp) {
                           mp.reset();
                       }
                   });
               }
               alertDialog = new AlertDialog.Builder(PlisiActivity.this).create();
               alertDialog.setTitle("Πληροφορίες Συστήματος");
               alertDialog.setMessage("Η πλύση σας έχει ολοκληρωθεί.\n \nΠατήστε 'ΟΚ' για έξοδο");
               alertDialog.setCanceledOnTouchOutside(false);
               alertDialog.setCancelable(false);
               alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {
                       Intent goBackToMainIntent = new Intent(getApplicationContext(), MainActivity.class);
                       goBackToMainIntent.putExtra("cleanStart", false);
                       goBackToMainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                       startActivity(goBackToMainIntent);
                   }
               });
               alertDialog.setIcon(R.drawable.my_check_dialog_img);
               alertDialog.show();
               TextView message = (TextView)alertDialog.findViewById(android.R.id.message);
               message.setTextSize(40);

               TextView buttonYes = (TextView)alertDialog.findViewById(android.R.id.button1);
               buttonYes.setTextSize(40);

               TextView buttonNo = (TextView)alertDialog.findViewById(android.R.id.button2);
               buttonNo.setTextSize(40);
               mTimerRunning = false;

           }
       }.start();
       mTimerRunning = true;
    }

    private void pauseTimer(){
        countDownTimer.cancel();
        mTimerRunning = false;

    }

    private void updateCountDownText(){
        int minutes = (int) (mTimeLeftMillis / 1000) / 60;
        int seconds = (int) (mTimeLeftMillis / 1000) % 60;

        String timeLeft = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
        timerValue.setText(timeLeft);

    }



    @Override
    protected void onStop() {

        if (sound1 != null){
            sound1.release();
        }
        if (sound2 != null){
            sound2.release();
        }
        if (sound3 != null){
            sound3.release();
        }

        super.onStop();
    }

    @Override
    public void onBackPressed() {
        alertDialog = new AlertDialog.Builder(PlisiActivity.this).create();
        alertDialog.setTitle("Πληροφορίες Συστήματος");
        alertDialog.setMessage("Είστε σίγουρος ότι θέλετε να ακυρώσετε την πλύση;");
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "ΝΑΙ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (sound1 != null){
                    sound1.release();
                }
                if (sound2 != null){
                    sound2.release();
                }
                countDownTimer.cancel();
                Intent goBackToMainIntent = new Intent(getApplicationContext(), MainActivity.class);
                goBackToMainIntent.putExtra("cleanStart", false);
                goBackToMainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(goBackToMainIntent);
                finish();

            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "ΟΧΙ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.setIcon(R.drawable.my_warning_dialog_img);
        alertDialog.show();
        TextView message = (TextView)alertDialog.findViewById(android.R.id.message);
        message.setTextSize(40);

        TextView buttonYes = (TextView)alertDialog.findViewById(android.R.id.button1);
        buttonYes.setTextSize(40);

        TextView buttonNo = (TextView)alertDialog.findViewById(android.R.id.button2);
        buttonNo.setTextSize(40);


    }
// if i want to save the time when the orientation changes

/*    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("millisLeft", mTimeLeftMillis);
        outState.putBoolean("timerRunning", mTimerRunning);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mTimeLeftMillis = savedInstanceState.getLong("millisLeft");
        mTimerRunning = savedInstanceState.getBoolean("timerRunning");
        updateCountDownText();
        if (mTimerRunning){
            startTimer(mTimeLeftMillis);
        }


    }*/
}

