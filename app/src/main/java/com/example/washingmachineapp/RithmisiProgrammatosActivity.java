package com.example.washingmachineapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;

public class RithmisiProgrammatosActivity extends AppCompatActivity {

    RadioGroup temperature, speed, programType, info;
    RadioButton temp, spd, type;
    Button apply, akiro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rithmisi_programmatos);
        Toolbar mToolbar =  (Toolbar) findViewById(R.id.myToolbar);
        mToolbar.setTitle("Ρύθμιση Προγράμματος");
        setSupportActionBar (mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final ArrayList<String> additionalInfo = new ArrayList<String>();




        apply = (Button)findViewById(R.id.button4);

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                temperature = (RadioGroup) findViewById(R.id.r1);
                speed = (RadioGroup) findViewById(R.id.r2);
                programType = (RadioGroup) findViewById(R.id.r3);
                info = (RadioGroup) findViewById(R.id.r4);

                if(((CheckBox)findViewById(R.id.checkBox)).isChecked()){
                    additionalInfo.add("Πρόπλυση");
                }
                if(((CheckBox)findViewById(R.id.checkBox2)).isChecked()){
                    additionalInfo.add("Χωρίς Τσαλάκωμα");
                }
                if(((CheckBox)findViewById(R.id.checkBox3)).isChecked()) {
                    additionalInfo.add("Επιπλέον Ξέβγαλμα");
                }
                if(((CheckBox)findViewById(R.id.checkBox4)).isChecked()) {
                    additionalInfo.add("Χωρίς Στύψιμο");
                }


                temp = (RadioButton) findViewById(temperature.getCheckedRadioButtonId());
                spd = (RadioButton) findViewById(speed.getCheckedRadioButtonId());
                type = (RadioButton) findViewById(programType.getCheckedRadioButtonId());

                ProgrammaPlisis prog = new ProgrammaPlisis((String)temp.getText(), (String)spd.getText(),(String)type.getText(),additionalInfo);
                Intent plisiIntent = new Intent(v.getContext(), PlisiActivity.class);
                plisiIntent.putExtra("message1", prog);
                plisiIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(plisiIntent);


            }
        });

        akiro = (Button)findViewById(R.id.button3);
        akiro.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
            }
        });

    }

    /**
     * when the back arrow from the toolbar is pressed this method is called.
     * it finishes the activity and goes back
     * @param item the back arrow
     * @return returns super
     */
    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home){
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }



}
