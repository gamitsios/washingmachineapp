package com.example.washingmachineapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class ProgrammaPlisis implements Serializable {
    private String temperature;
    private String speed;
    private String type;
    private ArrayList<String> additionalInfo = new ArrayList<String>();
    private int time;

    public ProgrammaPlisis(String temperature, String speed, String type, ArrayList<String> additionalInfo) {
        this.temperature = temperature;
        this.speed = speed;
        this.type = type;
        this.additionalInfo = additionalInfo;
        this.setTime(type);
    }

    public ProgrammaPlisis(String description){
        if (description.equalsIgnoreCase("βαμβακερά")){
            this.temperature = "60°C";
            this.speed = "Μέτρια";
            this.type = description;
            this.additionalInfo.add("Επιπλέον Ξέβγαλμα");
            this.time = 125;
        }else if (description.equalsIgnoreCase("συνθετικά")){
            this.temperature = "40°C";
            this.speed = "Μέτρια";
            this.type = description;
            this.additionalInfo.add("Χωρίς Τσαλάκωμα");
            this.time = 110;
        }else if (description.equalsIgnoreCase("μάλλινα")){
            this.temperature = "30°C";
            this.speed = "Χαμηλή";
            this.type = description;
            this.additionalInfo.add("Επιπλέον Ξέβγαλμα");
            this.time = 45;
        }else if (description.equalsIgnoreCase("ευαίσθητα")){
            this.temperature = "30°C";
            this.speed = "Χαμηλή";
            this.type = description;
            this.additionalInfo.add("Χωρίς Στύψιμο");
            this.additionalInfo.add("Χωρίς Τσαλάκωμα");
            this.time = 45;
        }else if (description.equalsIgnoreCase("λευκά")){
            this.temperature = "60°C";
            this.speed = "Υψηλή";
            this.type = description;
            this.additionalInfo.add("Πρόπλυση");
            this.additionalInfo.add("Επιπλέον Ξέβγαλμα");
            this.time = 120;
        }else if (description.equalsIgnoreCase("Πρόγραμμα γρήγορης πλύσης")){
            this.temperature = "30°C";
            this.speed = "Υψηλή";
            this.type = description;
            this.additionalInfo.add("Χωρίς Στύψιμο");
            this.time = 30;
        }else if (description.equalsIgnoreCase("οικονομικό πρόγραμμα")){
            this.temperature = "40°C";
            this.speed = "Μέτρια";
            this.type = description;
            this.time = 110;
        }else if (description.equalsIgnoreCase("κανονικό πρόγραμμα")){
            this.temperature = "60°C";
            this.speed = "Μέτρια";
            this.type = description;
            this.additionalInfo.add("Πρόπληση");
            this.time = 60;

        }

    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<String> getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(ArrayList<String> additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
    public String convertAdditionalInfo(ArrayList<String> additionalInfo){
        StringBuilder final_string = new StringBuilder();
        for (String s : additionalInfo){
            final_string.append(" | ").append(s);

        }
        final_string.deleteCharAt(1);
        final_string.append("  ");
        return final_string.toString();
    }

    public int getTime() {
        return time;
    }

    public void setTime(String description) {
        if (description.equalsIgnoreCase("βαμβακερά")){
            this.time = 125;
        }else if (description.equalsIgnoreCase("συνθετικά")){
            this.time = 110;
        }else if (description.equalsIgnoreCase("μάλλινα")){
            this.time = 45;
        }else if (description.equalsIgnoreCase("ευαίσθητα")){
            this.time = 45;
        }else if (description.equalsIgnoreCase("λευκά")){
            this.time = 120;
        }else if (description.equalsIgnoreCase("γρήγορη πλύση")){
            this.time = 30;
        }else if (description.equalsIgnoreCase("οικονομικό πρόγραμμα")){
            this.time = 110;
        }else if (description.equalsIgnoreCase("κανονικό πρόγραμμα")){
            this.time = 60;

        }

    }

    @Override
    public String toString() {
        return "ProgrammaPlisis{" +
                "temperature='" + temperature + '\'' +
                ", speed='" + speed + '\'' +
                ", type='" + type + '\'' +
                ", additionalInfo=" + additionalInfo +
                ", time=" + time +
                '}';
    }


}
