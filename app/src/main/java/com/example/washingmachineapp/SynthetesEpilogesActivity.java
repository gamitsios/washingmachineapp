package com.example.washingmachineapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import static com.example.washingmachineapp.MainActivity.EXP_USER;
import static com.example.washingmachineapp.MainActivity.SHARED_PREFS;

public class SynthetesEpilogesActivity extends AppCompatActivity {

    MediaPlayer sound1;
    MediaPlayer sound2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_synthetes_epiloges);

        SharedPreferences sharedPreferencesForSpeech = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        sound1 = MediaPlayer.create(SynthetesEpilogesActivity.this, R.raw.programmatasintheta);
        sound2 = MediaPlayer.create(SynthetesEpilogesActivity.this, R.raw.pathstepanw);



        Toolbar mToolbar =  (Toolbar) findViewById(R.id.myToolbar);
        mToolbar.setTitle("Σύνθετες Επιλογές");
        setSupportActionBar (mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (sharedPreferencesForSpeech.getBoolean(EXP_USER, true)){
            sound1.start();
            sound1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.reset();
                    sound2.start();
                    sound2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            mp.reset();
                        }
                    });
                }
            });
        }

        ListView listView = (ListView) findViewById(R.id.listView);
        final ArrayList<String> basikesEpilogesList= new ArrayList<String>();
        basikesEpilogesList.add("Βαμβακερά");
        basikesEpilogesList.add("Συνθετικά");
        basikesEpilogesList.add("Μάλλινα");
        basikesEpilogesList.add("Ευαίσθητα");
        basikesEpilogesList.add("Λευκά");


        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.myview, basikesEpilogesList);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (sound1 != null){
                    sound1.release();
                }
                if (sound2 != null){
                    sound2.release();
                }
                Intent plisiIntent = new Intent(view.getContext(), PlisiActivity.class);
                plisiIntent.putExtra("message", basikesEpilogesList.get(i));
                plisiIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(plisiIntent);


            }
        });


    }

    /**
     * when the back arrow from the toolbar is pressed this method is called.
     * it finishes the activity and goes back
     * @param item the back arrow
     * @return super
     */
    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home){
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        if (sound1 != null){
            sound1.release();
        }
        if (sound2 != null){
            sound2.release();
        }

        super.onStop();
    }
}
